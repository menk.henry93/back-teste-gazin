import * as dotenv from 'dotenv'

import express from 'express'

import { default as setupMiddlewares } from './shared/middleware'
import setupRoutes from './shared/routes'

dotenv.config()

const app = express()
setupMiddlewares(app)
setupRoutes(app)

export default app
