import { bodyParser } from './body-parser'
import { cors } from './cors'
import { Express } from 'express'

export default (app: Express): void => {
  app.use(bodyParser)
  app.use(cors)
}
