import app from '../../app'
import request from 'supertest'

describe('CORS Middlewares', () => {
  test('', async () => {
    app.get('/developers', (req, res) => {
      res.send()
    })
    await request(app)
      .get('/developers')
      .expect('access-control-allow-origin', '*')
      .expect('access-control-allow-headers', '*')
      .expect('access-control-allow-methods', '*')
  })
})
