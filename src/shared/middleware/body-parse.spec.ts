import request from 'supertest'
import app from '../../app'

describe('Body Parser Routes', () => {
  test('', async () => {
    app.post('/developers', (req, res) => {
      res.send(req.body)
    })
    await request(app)
      .post('/developers')
      .send({
        name: 'Matheus',
        sex: 'M',
        birthdate: '2000-10-10',
        age: '21',
        hooby: ''
      })
      .expect(200)
  })
})
