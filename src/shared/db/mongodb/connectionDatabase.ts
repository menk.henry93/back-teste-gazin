import 'dotenv/config'
import mongoose from 'mongoose'
let dataBase: any

export default function dataBaseConnetion(): mongoose.Connection {
  if (!dataBase) {
    mongoose.set('useUnifiedTopology', true)
    dataBase = mongoose
      .createConnection(<string>process.env.MONGO_URI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true
      })
      .on('connected', (): void => {})
  }

  return dataBase
}
