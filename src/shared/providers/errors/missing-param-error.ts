export class MissingParamError extends Error {
  constructor(message: string) {
    super(`Campo (${message}) é obrigatório`)
    Object.assign(this, message)
  }

  helperMessage(statusCode: number, message: string) {
    return {
      error: statusCode,
      message: message
    }
  }
}
