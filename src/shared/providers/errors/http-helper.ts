import { HttpResponse } from '../../protocols/http'

export const badRequest = (status: number, error: Error): HttpResponse => ({
  statusCode: status,
  body: error.message
})
