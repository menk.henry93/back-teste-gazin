import developersRoutes from '../../domain/developer/developers.routes'
import { Express, Router } from 'express'

export default (app: Express): void => {
  const router = Router()

  developersRoutes(router)

  app.use('/api', router)
}
