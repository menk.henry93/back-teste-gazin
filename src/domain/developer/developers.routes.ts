import { DeveloperController } from './developer.controller'
import { Router } from 'express'

const developerController = new DeveloperController()
const payload: string = '/developers/'

export default (router: Router): any => {
  router.get(payload, developerController.findAll)
  router.get(payload + ':id', developerController.findOne)

  router.post(payload, developerController.create)

  router.put(payload + ':id', developerController.edit)

  router.delete(payload + ':id', developerController.delete)
}
