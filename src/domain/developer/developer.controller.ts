import { badRequest } from '../../shared/providers/errors/http-helper'
import { Controller } from '../../shared/protocols/controller'
import { HttpResponse, HttpRequest } from '../../shared/protocols/http'
import { MissingParamError } from '../../shared/providers/errors/missing-param-error'
import { Request, Response } from 'express'
import Entity from './developer.entity'
import Interface from './developer.interface'

const error = new MissingParamError('Erro')

export class DeveloperController implements Controller {
  handle(httpRequest: HttpRequest): HttpResponse {
    const requiredFields = ['name', 'sex', 'birthdate', 'age', 'hobby']
    for (const field of requiredFields) {
      if (!httpRequest.body[field]) {
        return badRequest(404, new MissingParamError(field))
      }
    }
    return {
      statusCode: 200,
      body: httpRequest.body
    }
  }

  public async findAll(req: Request, res: Response): Promise<Interface> {
    let query: Object = {}
    let page = parseInt(<string>req.query.page)
    let itemsPerPage = parseInt(<string>req.query.itemsPerPage)
    if (req.query.findName) {
      query = {
        name: { $regex: '.*' + req.query.findName + '.*', $options: 'i' }
      }
    }

    let totalDocuments = await Entity.countDocuments(query)
    let totalPages = Math.ceil(totalDocuments / itemsPerPage)
    return await Entity.find(query)
      .sort({ name: 1 })
      .skip(page > 0 ? (page - 1) * itemsPerPage : 0)
      .limit(itemsPerPage)
      .then((developers: Interface[]): any => {
        res.status(200).json({
          totalDocuments: totalDocuments,
          totalPages: totalPages,
          data: developers
        })
      })
      .catch(() => {
        res
          .status(400)
          .json(
            error.helperMessage(400, 'Nenhum desenvolvedor foi encontrado!')
          )
      })
  }

  public async findOne(req: Request, res: Response): Promise<Interface> {
    return await Entity.findById(req.params.id)
      .then((developer: Interface[]): any => {
        res.status(200).json(developer)
      })
      .catch(() => {
        res
          .status(404)
          .json(
            error.helperMessage(
              404,
              'Verifique o id do desenvolvedor novamente'
            )
          )
      })
  }

  public async create(req: Request, res: Response): Promise<Interface> {
    return await Entity.create(req.body)
      .then((result: Interface): any => {
        res.status(201).json(result)
      })
      .catch((err) => {
        res.status(400).json(badRequest(400, new MissingParamError(err)))
      })
  }

  public async edit(req: Request, res: Response): Promise<Interface> {
    return await Entity.findByIdAndUpdate(
      req.params.id,
      {
        ...req.body
      },
      { new: true }
    )
      .then((result: Interface): any => {
        res.status(201).json(result)
      })
      .catch((err) => {
        res.status(400).json(badRequest(400, new MissingParamError(err)))
      })
  }

  public async delete(req: Request, res: Response): Promise<Interface> {
    return await Entity.findByIdAndDelete(req.params.id)
      .then((result: Interface): any => {
        res.status(201).json(result)
      })
      .catch(() => {
        res
          .status(400)
          .json(
            error.helperMessage(400, 'Não foi possível excluir o desenvolvedor')
          )
      })
  }
}
