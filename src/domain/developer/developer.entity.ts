import { default as dataBaseConnetion } from '../../shared/db/mongodb/connectionDatabase'
import { Schema } from 'mongoose'
import { v4 as uuidv4 } from 'uuid'

const developerSchema = new Schema(
  {
    _id: {
      type: String,
      default: uuidv4,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    sex: {
      type: String,
      required: true
    },
    birthdate: {
      type: Date,
      required: true
    },
    age: {
      type: Number,
      required: true
    },
    hobby: {
      type: String,
      required: true
    }
  },
  {
    timestamps: {
      createdAt: 'createdAt',
      updatedAt: 'updatedAt'
    }
  }
)

const dataBase = dataBaseConnetion()

export default dataBase.models.developer ||
  dataBase.model('developer', developerSchema)
