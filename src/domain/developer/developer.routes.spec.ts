import app from '../../app'
import request from 'supertest'

let PATH = '/api/developers/'
let idResult: string
let objectTest = {
  name: 'Matheus',
  sex: 'M',
  birthdate: '2000-10-10T00:00:00.000Z',
  age: 21,
  hobby: 'Play the piano'
}
let objectFake = {
  sex: 'M',
  birthdate: '2000-10-10T00:00:00.000Z',
  age: 21,
  hobby: 'Play the piano'
}

describe('Developer routes', () => {
  test('Should be insert', async () => {
    const objectPost = await request(app).post(PATH).send(objectTest)

    expect(objectPost.status).toBe(201)
    expect(typeof objectPost.body.age).toBe('number')
    expect(objectPost.body.name.length).toBeLessThanOrEqual(30)
    expect(objectPost.body.name).not.toBeNull()
    expect(objectPost.body.sex).not.toBeNull()
    expect(objectPost.body.birthdate).not.toBeNull()
    expect(objectPost.body.age).not.toBeNull()
    expect(objectPost.body.hobby).not.toBeNull()

    const objectError = await request(app).post(PATH).send(objectFake)

    expect(objectError.status).toBe(400)

    idResult = objectPost.body._id
  })

  test('Should be edit', async () => {
    const res = await request(app)
      .put(PATH + idResult)
      .send({ ...objectTest, name: 'Matheus [Alterado]', age: 25 })

    expect(res.body._id).not.toEqual(null)
    expect(res.status).toBe(201)
    expect(res.body.name).toEqual('Matheus [Alterado]')
    expect(res.body.age).toEqual(25)
    expect(typeof res.body).toBe('object')
  })

  test('Should be find one', async () => {
    const findId = await request(app)
      .get(PATH + idResult)
      .send()

    expect(findId.status).toBe(200)
    expect(findId.body._id).not.toBeNull()
  })

  test('Should be find all pagination', async () => {
    const result = await request(app)
      .get('/api/developers?page=1&itemsPerPage=8')
      .send()

    expect(result.status).toBe(200)
    expect(typeof result.body).toBe('object')
  })

  test('Should be delete', async () => {
    const deleteId = await request(app)
      .delete(PATH + idResult)
      .send()

    expect(deleteId.body._id).not.toBeNull()
    expect(deleteId.status).toBe(201)
  })
})
