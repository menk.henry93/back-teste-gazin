# Back Test Gazin with Typescript

This is experimental test with [Typescript](https://www.typescriptlang.org/).

## Description

Using a simple design patterns for to build an API.

## Clone the project

First of all you should download the project with this command:

```bash
git clone https://gitlab.com/menk.henry93/back-teste-gazin
```

## Install the dependency

You need to install the dependencies.

Use NPM:

```bash
npm install
```

or YARN:

```bash
yarn
```

## Start Docker:

For go up the database in docker you need to run this commands:

```bash
docker-compose up --build
```

and after this, follow the next step.

## Start project:

To run the application:

Use NPM:

```bash
npm run start
```

or YARN:

```bash
yarn start
```

## Start tests:

You could to run the tests too, with this commands:

If you use NPM:

```bash
npm run test
```

or YARN:

```bash
yarn test
```
